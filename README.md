# Stereo-scenes for pbrt-v3

## Overview

This repository includes a number of example scenes and
data for use with the [pbrt-v3](https://github.com/mmp/pbrt-v3) renderer,
which corresponds to the system described in the third edition of
_Physically Based Rendering_, by Matt Pharr, Wenzel Jakob, and Greg
Humphreys. (See also the [pbrt website](http://pbrt.org).)


## Data Sets

In addition to example scenes, there is some useful data for use with the
system.

* [bsdfs/](bsdfs/): this directory includes a variety of bidirectional scattering
  distribution functions (BSDFs) for use with the `FourierMaterial`. See, for
  example, the [coffee-splash](coffee-splash) scene for use of such a BSDF in a scene.
  * New versions of BSDFs for use with `FourierMaterial` can be generated
    with [layerlab](https://github.com/wjakob/layerlab/).

* [lenses/](lenses/): lens description files for a handful of real-world lens
  systems, for use with the `RealisticCamera`. See the scenes
  [villa/villa-photons.pbrt](villa/villa-photons.pbrt) and
  [sanmiguel/f6-17.pbrt](sanmiguel/f6-17.pbrt) for examples of their use.

* [spds/](spds/): measured spectral power distributions for a variety of standard
  illuminants, light sources, metals, and the squares of the Macbeth color
  checker.

## Stereo scenes and synthesis images

All synthesis images generated are of size `1920 x 1080` and saved into `.rawls` format for each sample estimated.

**Note:** `xxxx` means left or right eye view.

| Scene folder            | Filename                                 | Integrator | Sampler | Samples                  | Generated | Experiment step | Update reference  |
|-------------------------|------------------------------------------|------------|---------|--------------------------|-----------|-----------------|-------------------|
| `bathroom`              | `p3d_bathroom-stereoscopic-xxxx.pbrt`    | path       | random  | 1 to 10000               |  ✅       |   200           |   No              |
